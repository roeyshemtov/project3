#include "Date.h"
#include <iostream>
#include <string>

using namespace std;

Date::Date(int day, int month, int year)
{
	this->day = day;
	this->month = month;
	this->year = year;
}

Date::Date(const Date & date)
{
	this->day = date.day;
	this->month = date.month;
	this->year = date.year;
}

Date::Date()
{
	this->day = 1;
	this->month = 1;
	this->year = 1970;
}

void Date::setDay(int day)
{
	this->day = day;
}

void Date::setMonth(int month)
{
	this->month = month;
}

void Date::setYear(int year)
{
	this->year = year;
}

bool Date::isLeapYear(int year) const
{
	if (year % 4 != 0) return false;
	else if (year % 100 != 0) return true;
	else if (year % 400 != 0) return false;
	else return true;
}

void Date::printDate() const
{
	if (this->day < 0 || this->day > 31)
	{
		cout << "Illegal day for month";
		if (this->month < 0 || this->month > 12)
		{
			cout << "Illegal month";
		}
		return;
	}
	if (this->month < 0 || this->month > 12)
	{
		cout << "Illegal month";
		if (this->day < 0 || this->day > 31)
		{
			cout << "Illegal day for month";
		}
		return;
	}
	if (this->month == 4 && this->day == 31)
	{
		cout << "Illegal day for month";
		return;
	}
	if (this->day == 29 && this->month == 2)
		if (!isLeapYear(this->year))
		{
			cout << "Not a leap year";
			return;
		}

	if (this->month > 10) {
		if (this->day > 10)
			cout << this->day << "/" << this->month << "/" << this->year;
		else cout << "0" << this->day << "/" << this->month << "/" << this->year;
	}
	else if (this->day > 10)
		cout << this->day << "/0" << this->month << "/" << this->year;
	else cout << "0" << this->day << "/0" << this->month << "/" << this->year;
}

void Date::toOs(ostream& os) const
{
	this->printDate();
}

void Date::toIs(istream & in)
{
	string str;
	int posNum, posSlash;
	int d, m, y;
	getline(in, str);
	posNum = str.find_first_of("-0123456789");
	posSlash = str.find_first_of('/') - posNum;
	d = getNumFromString(str, posNum, posSlash);
	str = str.substr(posSlash + 1);
	m = getNumFromString(str, posNum, posSlash);
	if ((d >= 10 && m < 10) || (d < 10 && m >= 10 )|| (d < 10 && m < 0))
		str = str.substr(posSlash);
	else str = str.substr(posSlash + 1);
	y = getNumFromString(str, posNum, -1);
	this->day = d;
	this->month = m;
	this->year = y;
}

int Date::getNumFromString(string str, int pos1, int pos2)
{
	int num;
	if (str.at(0) == '-')
	{
		num = stoi(str.substr(1, pos2));
		num *= -1;
	}
	else if (pos2 == -1)
		num = stoi(str.substr(pos1));
	else num = stoi(str.substr(pos1, pos2));
	return num;
}

bool Date::operator==(const Date & date)
{
	return this->year == date.year &&
		this->month == date.month &&
		this->day == date.day;
}

bool Date::operator!=(const Date & date)
{
	return !(this->operator==(date));
}


bool Date::operator<=(const Date & date)
{
	return !(this->operator>(date));
}

bool Date::operator>=(const Date & date)
{
	return !(this->operator<(date));
}

bool Date::operator>(const Date & date)
{
	if (this->year == date.year)
		if (this->month == date.month)
			return this->day > date.day;
		else return this->month > date.month;
	else
		return this->year > date.year;
}

bool Date::operator<(const Date & date)
{
	if (this->year == date.year)
		if (this->month == date.month)
			return this->day < date.day;
		else return this->month < date.month;
	else
		return this->year < date.year;
}

istream & operator>>(istream & in, IPrintable & i)
{
	i.toIs(in);
	return in;
}

ostream& operator<<(ostream& os, const IPrintable& i)
{
	i.toOs(os);
	return os;
}