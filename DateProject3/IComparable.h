#pragma once

template <class T>
class IComparable{
public:
	virtual bool operator==(const T&) =0;
	virtual bool operator!=(const T&) =0;
	virtual bool operator<=(const T&) = 0;
	virtual bool operator>=(const T&) =0;
	virtual bool operator>(const T&) =0;
	virtual bool operator<(const T&) =0;
};

