#pragma once
#include <iostream>
using namespace std;

class IPrintable {
public:
	virtual void toIs (istream& in) = 0;
	friend istream & operator >> (istream &in, IPrintable& i);
	virtual void toOs(ostream& os) const = 0;
	friend ostream& operator <<(ostream& os, const IPrintable& i);
};