#pragma once
#include "IComparable.h"
#include "IPrintable.h"
using namespace std;
class Date : public IPrintable, public IComparable<Date> {
public:
	Date(int day, int month, int year);
	Date(const Date &date);
	Date();
	void setDay(int day);
	void setMonth(int month);
	void setYear(int year);
	// Inherited via IComparable
	virtual bool operator==(const Date & date) override;
	virtual bool operator!=(const Date & date) override;
	virtual bool operator<=(const Date & date) override;
	virtual bool operator>=(const Date & date) override;
	virtual bool operator>(const Date & date) override;
	virtual bool operator<(const Date & date) override;
	// Inherited via IPrintable
	friend ostream& operator <<(ostream& os, const IPrintable& i);
	virtual void toOs(ostream & os) const override;
	friend istream & operator >> (istream &in, IPrintable& i);
	virtual void toIs(istream& in) override;


private:
	int day;
	int month;
	int year;
	bool isLeapYear(int year) const;
	int getNumFromString(string str, int pos1, int pos2);
	void printDate() const;
};
