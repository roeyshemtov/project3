#pragma once
#pragma once
#include "IComparable.h"
#include "IPrintable.h"
#include <iostream>
#include <string>

using namespace std;
template<typename T>
class Interval
{
public:
	T leftElement;
	T rightElement;
	bool empty;
	
	Interval() {
		empty = true;
	};
	
	Interval(T l, T r) {
		leftElement = l;
		rightElement = r;
		empty = false;
	}
	~Interval() {}

	friend ostream& operator <<(ostream& os, const Interval<T>& i) {
		if (i.empty)
			cout << "EMPTY";
		else if (i.leftElement > i.rightElement)
			cout << "Invalid interval";
		else
			cout << "(" << i.leftElement<< ", " << i.rightElement << ")";
		return os;
	}
	
	bool contains(T value) {
		return value > this->leftElement && value < this->rightElement ? true : false;
	}

	bool isBefore(Interval<T> &interval) {
		return this->rightElement <= interval.leftElement ? true : false;
	}

	bool isAfter(Interval<T> &interval) {
		return this->leftElement >= interval.rightElement ? true : false;
	}

	bool intersects(Interval<T> &interval) {
		return (this->leftElement > interval.leftElement && this->leftElement < interval.rightElement) ||
			(this->rightElement > interval.leftElement && this->rightElement < interval.rightElement) ?
			true : false;
	}
	
	Interval<T> operator&&(Interval<T> interval) {
		if (this->leftElement > interval.leftElement && this->leftElement < interval.rightElement) {
			if (this->rightElement < interval.rightElement)
				return Interval<T>(this->leftElement, this->rightElement);
			else
				return Interval<T>(this->leftElement, interval.rightElement);
		}
		else if (this->rightElement > interval.leftElement && this->rightElement < interval.rightElement) {
			if(this->leftElement < interval.leftElement)
				return Interval<T>(interval.leftElement, this->rightElement);
			else
				return Interval<T>(this->rightElement, interval.rightElement);

		}
		return Interval<T>();
	}

	Interval<T> operator||(Interval<T> interval) {
		if (this->leftElement > interval.leftElement && this->leftElement < interval.rightElement)
			return Interval<T>(min(this->leftElement, interval.leftElement), max(this->rightElement, interval.rightElement));
		if (this->rightElement > interval.leftElement && this->rightElement < interval.rightElement)
			return Interval<T>(min(this->leftElement, interval.leftElement), max(this->rightElement, interval.rightElement));
		return Interval<T>(1, -1);
	}

	T min(T v1, T v2) {
		return v1 < v2 ? v1 : v2;
	}
	T max(T v1, T v2) {
		return v1 > v2 ? v1 : v2;
	}

};

